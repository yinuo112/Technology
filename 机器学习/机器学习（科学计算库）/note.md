## 【机器学习】科学库使用【附代码文档】 


![](banner/1.jpg)

### 机器学习（科学计算库）完整教程（附代码资料）主要内容讲述：机器学习（常用科学计算库的使用）基础定位、目标，机器学习概述定位,目标,学习目标,学习目标。机器学习概述，1.3 人工智能主要分支学习目标,学习目标,1 主要分支介绍,2 小结。机器学习概述，1.5 机器学习算法分类学习目标,学习目标,1 监督学习,2 无监督学习。机器学习概述，1.7 Azure机器学习模型搭建实验学习目标,学习目标,Azure平台简介,学习目标。Matplotlib，3.2 基础绘图功能 — 以折线图为例学习目标,学习目标,1 完善原始折线图 — 给图形添加辅助功能,2 在一个坐标系中绘制多个图像。Matplotlib，3.3 常见图形绘制学习目标,学习目标,1 常见图形种类及意义,2 散点图绘制。Numpy，4.2 N维数组-ndarray学习目标,学习目标,1 ndarray的属性,2 ndarray的形状。Numpy，4.3 基本操作学习目标,学习目标,1 生成数组的方法,2 数组的索引、切片。Numpy，4.4 ndarray运算学习目标,学习目标,问题,1 逻辑运算。Numpy，4.6 数学：矩阵学习目标,学习目标,1 矩阵和向量,2 加法和标量乘法。Pandas，5.1Pandas介绍学习目标,学习目标,1 Pandas介绍,2 为什么使用Pandas。Pandas，5.3 基本数据操作学习目标,学习目标,1 索引操作,2 赋值操作。Pandas，5.6 文件读取与存储学习目标,学习目标,1 CSV,2 HDF5。Pandas，5.8 高级处理-数据离散化学习目标,学习目标,1 为什么要离散化,2 什么是数据的离散化。Pandas，5.12 案例学习目标,学习目标,1 需求,2 实现。





 # 个人博客获取：[https://www.666mao.com/sku?skuId=4](https://www.666mao.com/sku?skuId=4) 

 ### 并且有更多其他资料

# 网盘地址直接获取：[https://pan.baidu.com/s/1eH49MknzRYLrEGq2-5zqPg?pwd=mdg4](https://pan.baidu.com/s/1eH49MknzRYLrEGq2-5zqPg?pwd=mdg4)

![](content/1.jpg)![](content/2.jpg)![](content/3.jpg)![](content/4.jpg)![](content/5.jpg)![](content/6.jpg)![](content/7.jpg)![](content/8.jpg)![](content/9.jpg)![](content/10.jpg)![](content/11.jpg)![](content/12.jpg)![](content/13.jpg)![](content/14.jpg)![](content/15.jpg)



